Biomaj
======

A role to install Biomaj As Microservice from https://github.com/genouest/biomaj-docker

Like it is documented here:
https://biomaj.genouest.org/with-docker/


Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)

[Dependencie Variables](vars/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/playbook.yml)


License
-------

[License](LICENSE)
