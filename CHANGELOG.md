<a name="unreleased"></a>
## [Unreleased]


<a name="0.2.4"></a>
## [0.2.4] - 2019-11-12
### Add
- add an option to limit docker log


<a name="0.2.3"></a>
## [0.2.3] - 2019-10-28
### Add
- add a restart always for compose as production

### Find
- find service list if not defined in var

### Fix
- fix default value for path + enable prometheus


<a name="0.2.2"></a>
## [0.2.2] - 2019-10-24
### Remove
- remove version for molecule and gitlab test
- remove version for molecule test
- remove version from meta


<a name="0.2.1"></a>
## [0.2.1] - 2019-10-24
### Add
- add a .gitignore
- add a limit to history


<a name="0.2.0"></a>
## [0.2.0] - 2019-10-23
### Add
- add a smart comment

### Allow
- allow use of env variable for biomaj path

### Fix
- fix syntax: removing )
- fix syntax again: add the missing \
- fix syntax: add the missing }}

### Release
- Release 0.2.0

### Try
- try to enable alu bank by default
- try to use shared runner path to mount volume in docker dind


<a name="0.1.2"></a>
## [0.1.2] - 2019-10-23
### Disable
- disable .env debug

### Fix
- fix ansible loop syntax for bank name

### Remove
- remove useless space

### Split
- split bank properties and process variable


<a name="0.1.1"></a>
## [0.1.1] - 2019-10-22
### Add
- add a retry on enable bank

### Release
- Release 0.1.1


<a name="0.1.0"></a>
## [0.1.0] - 2019-10-21
### Add
- add an option to enable bank/show/history
- add a link to have biomaj data where we want
- add an option to force remove user + try to update proxy version
- add variable for mail
- add user and default pass in .env file
- add user create and bank download
- add a variable for services user and pass
- add timeout + log + env in molecule conf
- add a cat of env file for debug
- add molecule test with allow failure

### Disable
- disable git force update + use last release of biomaj
- disable get of ip proxy as it use too much {

### Fix
- fix syntax
- fix replace syntax
- fix link creation
- fix another undefined variable
- fix undefined variable

### Ignore
- ignore error on user deletion

### Readd
- readd /var/run/docker.sock to work in local + disable molecule on gitlab

### Remove
- remove useless -T option to docker-compose exec

### Save
- save public proxy ip

### Store
- store api key in file

### Try
- try to fix again again replace syntax
- try to force git update
- try to use alpine traefik
- try to fix again replace syntax
- try to fix loop for bank
- try fix ansible loop syntax
- try again to disable molecule stage
- try to disable molecule stage
- try again to show .env value in log
- try with explicit biomaj release shahikorma-v0
- try to fix stages gitlab-ci syntax

### Use
- use replace to replace content of docker compose
- use list with subelement for bank + check if user already exist
- use seq for gitlab.ci
- use python3 + install dep for package installation

### Reverts
- remove useless -T option to docker-compose exec


<a name="0.0.3"></a>
## [0.0.3] - 2019-10-16
### Add
- add a variable for dockerhost

### Disable
- disable pip install docker-compose as it is installed by docker install role

### Try
- try with an explicit list of service without promotheus
- try to test without molecule
- try an ugly hack for test in gitlab runner

### Reverts
- disable pip install docker-compose as it is installed by docker install role
- try an ugly hack for test in gitlab runner


<a name="0.0.2"></a>
## [0.0.2] - 2019-10-02
### Add
- add changelog
- add docker compose command

### Fix
- fix path for docker compose

### Remove
- remove epel from molecule requirement
- remove quote as it is not supported yet in .env by compose

### Try
- try to build with overlay as docker driver to solv gitlab runner disk space issue

### Update
- Update Readme to add info about biomaj role

### Reverts
- try to build with overlay as docker driver to solv gitlab runner disk space issue


<a name="0.0.1"></a>
## 0.0.1 - 2019-09-27
### Add
- add some space for lint
- add .env template + create some data dir + install biomaj cli
- add empty Fedora Vars

### First
- First Commit

### Remove
- remove Fedora as it .......
- remove CentOS as it .....
- remove /var/run/docker.sock mount

### Use
- use docker in docker


[Unreleased]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.2.4...HEAD
[0.2.4]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.2.3...0.2.4
[0.2.3]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.2.2...0.2.3
[0.2.2]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.2.1...0.2.2
[0.2.1]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.1.2...0.2.0
[0.1.2]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.0.3...0.1.0
[0.0.3]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.0.2...0.0.3
[0.0.2]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-biomaj/compare/0.0.1...0.0.2
